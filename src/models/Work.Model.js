import mongoose, { Schema } from 'mongoose'

const workSchema = new Schema({
    name: { type: String, required: true, trim: true },
    status: { type: String, required: true, trim: true }
}, { timestamps: true, versionKey: false })

const Work = mongoose.model('Work', workSchema)

export default Work;