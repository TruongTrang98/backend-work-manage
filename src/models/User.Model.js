import mongoose, { Schema } from "mongoose";
import bcrypt from "bcrypt";

const userSchema = new Schema(
  {
    email: { type: String, required: true, trim: true },
    password: { type: String, required: true, trim: true },
    name: { type: String, required: true, trim: true },
    workId: [{ type: Schema.Types.ObjectId, ref: 'Work' }]
  },
  { timestamps: true, versionKey: false }
);

userSchema.pre("save", function (next) {
  var user = this;
  if (this.isModified("password") || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          return next(err);
        }
        console.log(hash);
        user.password = hash;
        next();
      });
    });
  }
});

userSchema.pre("updateOne", function (next) {
  var user = this;
  bcrypt.hash(user._update.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user._update.password = hash;
    next();
  });
});

const User = mongoose.model("User", userSchema);

export default User;
