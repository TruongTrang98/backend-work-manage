import bcrypt from 'bcrypt'

const comparePassword = async (passlogin, passdb, cb) => {
    bcrypt.compare(passlogin, passdb, (err, isMatch) => {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}

export default comparePassword