import mongoose from "mongoose";

const user = process.env.DB_USER;
const pass = process.env.DB_PASS;

const DB_URI = `mongodb://${user}:${pass}@ds363058.mlab.com:63058/work-manage`;

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: false, // Don't build indexes
  poolSize: 10, // Maintain up to 10 socket connections
  serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  family: 4 // Use IPv4, skip trying IPv6
};

mongoose
  .connect(DB_URI, options)
  .then(
    () => console.log("Database connection successfully"),
    error => {
      console.log("Can not connect database. Error", error)
      process.exit(1)
    }
  )
  .catch(error => {
    console.log(error)
    process.exit(1)
  });
