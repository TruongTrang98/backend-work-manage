import jwt from "jsonwebtoken";
const secketKey = process.env.SECRETKEY;

const checkToken = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"];
  // console.log(token);
  if (token) {
    if (token.startsWith("Bearer ")) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    jwt.verify(token, secketKey, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: "Token is invalid"
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: "Auth token is not supplied!"
    });
  }
};

export default checkToken;
