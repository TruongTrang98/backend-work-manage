import express from "express";
import checkToken from "../middlewares/verify_token";
import UserController from "../controllers/User.Controller";
import WorksController from "../controllers/Works.Controller";
const router = express.Router();

/**
 * USER
 */
router.get("/accounts", UserController.accounts);
router.post("/login", UserController.login);
router.post("/signUp", UserController.signUp);
router.post("/updatePassword", checkToken, UserController.updatePassword);

/**
 * WORK
 */
router.use(checkToken);
router.get("/works", WorksController.works);
router.post("/createNewWork", WorksController.createNewWork);
router.post("/updateWork/:_id", WorksController.updateWork);
router.get("/deleteWork/:_id", WorksController.deleteWork);
router.get("/deleteAll", WorksController.deleteAll);
router.get("/deleteCompleted", WorksController.deleteCompleted);

export default router;
