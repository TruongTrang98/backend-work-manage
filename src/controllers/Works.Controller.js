import Work from "../models/Work.Model";
import User from "../models/User.Model";

class WorksController {
  async works(req, res) {
    const currentUser = req.decoded.data;
    const result = await User.findOne({ email: currentUser.email }).populate(
      "workId"
    );
    res.json(result.workId);
  }

  async createNewWork(req, res) {
    const { name, status } = req.body;
    console.log(name, status);
    const newWork = await Work({
      name,
      status,
    });
    const result = await newWork.save();
    console.log(newWork, result);
    const updWorkUser = await User.update(
      { _id: req.decoded.data._id },
      { $push: { workId: result._id } }
    );

    res.send(updWorkUser);
  }

  async updateWork(req, res) {
    const _workId = req.params._id;
    const { name, status } = req.body.updData;
    const currentUser = await User.findById(req.decoded.data._id);
    const existedWork = await Work.findById(_workId);
    // console.log(!!existedWork, currentUser);
    if (!!existedWork && currentUser.workId.includes(_workId)) {
      const result = await Work.updateOne({ _id: _workId }, { name, status });
      return res.json(result);
    }
    return res.json({
      success: false,
      message: "Can not update work",
    });
  }

  async deleteWork(req, res) {
    const _workId = req.params._id;
    const currentUser = await User.findById(req.decoded.data._id);
    const existedWork = await Work.findById(_workId);
    if (!!existedWork && currentUser.workId.includes(_workId)) {
      await Work.deleteOne({ _id: _workId });
      await User.update(
        { email: currentUser.email },
        {
          $pull: { workId: _workId },
        }
      );
      return res.json({
        success: true,
        message: "Deleted",
      });
    }
    return res.json({
      success: false,
      message: "Can not delete work",
    });
  }

  async deleteAll(req, res) {
    const worksDel = await User.findById(req.decoded.data._id)
    worksDel.workId.map(async(work) => {
      await Work.findByIdAndRemove(work)
    })    
    await User.update({ email: req.decoded.data.email }, { workId: [] });
    return res.json({
      success: true,
      message: "Deleted",
    });
  }

  async deleteCompleted(req, res) {
    const currentUser = req.decoded.data;
    const data = await User.findOne({ email: currentUser.email }).populate(
      "workId"
    );
    data.workId.map(async (item) => {
      // console.log(item);
      if (item.status == "Done") {
        await Work.deleteOne({ _id: item._id });
        await User.update(
          { email: currentUser.email },
          {
            $pull: { workId: item._id },
          }
        );
      }
    });
    const result = await User.findOne({ email: currentUser.email }).populate(
      "workId"
    );
    // console.log(result);
    return res.json({
      success: true,
      message: "Deleted",
      works: result.workId
    });
  }
}

export default new WorksController();
