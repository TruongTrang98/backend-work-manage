import jwt from "jsonwebtoken";
import comparePassword from '../helpers/bcrypt'
import User from "../models/User.Model";

const secketkey = process.env.SECRETKEY;
const tokenLife = process.env.TOKEN_LIFE;

class UserController {
  async accounts(_, res) {
    const accounts = await User.find({});
    res.send(accounts);
  }

  async signUp(req, res) {
    const { email, password, name } = req.body;
    const existedAccount = await User.find({ email });
    if (existedAccount.length <= 0) {
      const newUser = await User({
        email,
        password,
        name
      });
      const result = await newUser.save();
      res.send(result);
    } else {
      res.send("Account exited");
    }
  }

  /**
   * LOGIN
   */
  async login(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      let token = undefined;
      if (user) {
        await comparePassword(password, user.password, (err, isMatch) => {
          if (isMatch && !err) {
            token = jwt.sign({ data: user }, secketkey, {
              expiresIn: tokenLife
            });
            return res.json({
              success: true,
              message: "Authentication successful!",
              token,
              user
            });
          } else {
            return res.json({
              success: false,
              message: "Invalid username or password! Please check again"
            });
          }
        });
      } else {
        return res.json({
          success: false,
          message: "Invalid username or password! Please check again"
        });
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  async updatePassword(req, res) {
    const currentUser = req.decoded.data;
    const { oldPassword, newPassword } = req.body;
    const user = await User.findOne({ email: currentUser.email });
    await comparePassword(oldPassword, user.password, async (err, isMatch) => {
      if (isMatch && !err) {
        if (user) {
          await User.updateOne({ email: currentUser.email }, { password: newPassword })
          return res.json({
            success: true,
            message: 'Update password successfully!'
          })
        }
        return res.json({
          success: false,
          message: 'Can not update password!'
        })
      } else {
        return res.json({
          success: false,
          message: 'Wrong old Password!'
        })
      }
    })
  }
}

export default new UserController();
