import express from 'express'
import cors from 'cors'
import morgan from 'morgan'

import router from './routes/index'

const PORT = process.env.PORT || 8080
const app = express()

app.use(morgan('common'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use('/api', router)


app.listen(PORT, () => console.log(`Server is running at port ${PORT}`))
