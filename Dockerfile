FROM node:12.16.1-alpine

WORKDIR /backend

COPY . .

RUN npm install

CMD ["npm", "start"]
