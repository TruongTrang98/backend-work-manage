WORK MANAGER API

## How to use

URL: /api/*

/accounts => List all accounts in system 
/login => login with existed account, when logined, system will return token for client 
/signUp => Sign up account with "email, pasword and name"
/updatePassword => Change password 

## This url below use token to verify, you must login before 
/works => List works
/createWork => Create new work with work contain "name, status"
/updateWork/:_id => Update work with param is _id of work and body is "name, status"
/deleteWork/:_id => Delete work with work's _id in param
/deleteAll => Delete all work 
/deleteCompleted => Delete all work with status = true
